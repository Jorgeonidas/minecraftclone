﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TextureType{
	air, grass, rock
}

public class Chunk : MonoBehaviour {
	//variables necesarias para armar el Voxel
	//lista de las coordenadas de los vertices que armaran los triangunlos
	//el ancho de la textura en la cuadricula de texture atlas
	private List<Vector3> newVertices = new List<Vector3>();
	//lista que enumerara los triangulos que constituiran la superficie (un cuadrado esta dividido por dos triangulo)
	private List<int> newTriangles = new List<int>();
	//la textura de la superficie
	private List<Vector2> newUV = new List<Vector2>();

	//toda esta informacion sera almacenada por la malla (mesh)
	private Mesh mesh;
	private MeshCollider chunkCollider;
	private float textureWidth = 0.083f; 
	private int faceCount;
	private World world;
	private int chunkSize = 16;
	private int chunkX;
	private int chunkY;
	private int chunkZ;
	private GameObject worldGameObject;	
	private bool isUpdate = false;

	//texturas 
	//tomara la textura del atlas cada textura esta ubicada en una coordenada x y (0,0) se encuentra abajo y a la izquierda
	private Vector2 grassTop = new Vector2(1,11);
	private Vector2 grassSide = new Vector2(0,10);
	private Vector2 rock = new Vector2(7,8);

	public int ChunkSize{
		get{
			return chunkSize;
		}
		set{
			chunkSize = value;
		}
	}

	public int ChunkX{
		get{
			return chunkX;
		}
		set{
			chunkX = value;
		}
	}
	public int ChunkY{
		get{
			return chunkY;
		}
		set{
			chunkY = value;
		}
	}
	public int ChunkZ{
		get{
			return chunkZ;
		}
		set{
			chunkZ = value;
		}
	}

	public GameObject WorldGameObject{
		get{
			return worldGameObject;
		}
		set{
			worldGameObject = value;
		}
	}

	public bool IsUpdate{
		get{
			return isUpdate;
		}
		set{
			isUpdate = value;
		}
	}
	// Use this for initialization
	void Start () {
		world = worldGameObject.GetComponent<World>() as World;
		mesh = GetComponent<MeshFilter>().mesh;
		chunkCollider = GetComponent<MeshCollider>();
		GenerateMesh();
	}
	
	// Update is called once per frame
	void LateUpdate () {
		if(IsUpdate){
			GenerateMesh();
			IsUpdate = false;
		}
	}

	public void GenerateMesh(){
		for(int x = 0; x < chunkSize; x++){
			for( int y = 0; y< chunkSize; y++){
				for(int z = 0; z < chunkSize; z++ ){
					//si no es aire renderiza la maya y textura
					if (Block(x,y,z) != (byte)TextureType.air.GetHashCode() )
					{
						//Bloque encima es aire
						if(Block(x,y+1,z) == (byte)TextureType.air.GetHashCode()){
							CubeTop(x,y,z,Block(x,y,z));
						}
						//Bloque debajo es aire
						if(Block(x,y-1,z) == (byte)TextureType.air.GetHashCode()){
							CubeBottom(x,y,z,Block(x,y,z));
						}
						//Bloque este es aire
						if(Block(x+1,y,z) == (byte)TextureType.air.GetHashCode()){
							CubeEast(x,y,z,Block(x,y,z));
						}
						//Bloque Oeste es aire
						if(Block(x-1,y,z) == (byte)TextureType.air.GetHashCode()){
							CubeWest(x,y,z,Block(x,y,z));
						}
						//Bloque Norte es aire
						if(Block(x,y,z+1) == (byte)TextureType.air.GetHashCode()){
							CubeNorth(x,y,z,Block(x,y,z));
						}
						//Bloque Sur es aire
						if(Block(x,y,z-1) == (byte)TextureType.air.GetHashCode()){
							CubeSouth(x,y,z,Block(x,y,z));
						}
					}
				}
			}
		}
		UpdateMesh();
	}

	//metodos
	//actualizamos la maya dados los datos sobre posicion de verticesr y textura
	void UpdateMesh(){
		//limpio la maya
		mesh.Clear();
		//agrego la nueva data a la maya (coordenadas, vertices, UV's)
		mesh.vertices = newVertices.ToArray();
		mesh.uv = newUV.ToArray();
		mesh.triangles = newTriangles.ToArray();
		mesh.RecalculateNormals();

		//asigno el collidermesh a nuestra malla con la nueva data
		chunkCollider.sharedMesh = null;
		chunkCollider.sharedMesh = mesh;//crea el collider y la textura de la malla
		
		//limpio la data
		newVertices.Clear();
		newUV.Clear();
		newTriangles.Clear();
		faceCount = 0;
	}

	void CubeTop(int x, int y , int z, byte block){//la variable typo byte nos indicara cual textura va a ser
		//agregamos 4 puntos al arreglo de vertices de la superficue tope del cubo (supongamos que le pasamos el origen de coordenadas)
		newVertices.Add(new Vector3(x, y, z+1)); 	//(0,0,1)
		newVertices.Add(new Vector3(x+1, y, z+1)); 	//(1,0,1)
		newVertices.Add(new Vector3(x+1, y, z));	//(1,0,0)
		newVertices.Add(new Vector3(x, y, z));		//(0,0,0)

		Vector2 texturePosition = new Vector2(0,0);
		
		//asignar textura
		if(block == (byte)TextureType.rock.GetHashCode()){
			texturePosition = rock;
		}else if(block == (byte)TextureType.grass.GetHashCode()){
			texturePosition = grassTop;
		}

		Cube(texturePosition);

		//guarda las esquinas de la textura que esta en el atlas
		
	}
	void CubeNorth(int x, int y , int z, byte block){//la variable typo byte nos indicara cual textura va a ser
		//agregamos 4 puntos al arreglo de vertices de la superficue tope del cubo (supongamos que le pasamos el origen de coordenadas)
		newVertices.Add(new Vector3(x+1, y-1, z+1)); 	//(1,-1,1)
		newVertices.Add(new Vector3(x+1, y, z+1)); 	//(1,0,1)
		newVertices.Add(new Vector3(x, y, z+1));	//(0,0,1)
		newVertices.Add(new Vector3(x, y-1, z+1));		//(0,-1,1)

		Vector2 texturePosition = setSideTextures(x,y,z,block);

		Cube(texturePosition);

		//guarda las esquinas de la textura que esta en el atlas
		
	}

	void CubeEast(int x, int y , int z, byte block){//la variable typo byte nos indicara cual textura va a ser
		//agregamos 4 puntos al arreglo de vertices de la superficue tope del cubo (supongamos que le pasamos el origen de coordenadas)
		newVertices.Add(new Vector3(x+1, y-1, z)); 		//(1,-1,0)
		newVertices.Add(new Vector3(x+1, y, z)); 		//(1,0,0)
		newVertices.Add(new Vector3(x+1, y, z+1));		//(1, 0, 1)
		newVertices.Add(new Vector3(x+1, y-1, z+1)); 	//(1,-1,1)


		Vector2 texturePosition = setSideTextures(x,y,z,block);

		Cube(texturePosition);

		//guarda las esquinas de la textura que esta en el atlas
		
	}

	void CubeSouth(int x, int y , int z, byte block){//la variable typo byte nos indicara cual textura va a ser
		//agregamos 4 puntos al arreglo de vertices de la superficue tope del cubo (supongamos que le pasamos el origen de coordenadas)
		newVertices.Add(new Vector3(x, y-1, z)); 	//(0,-1,0)
		newVertices.Add(new Vector3(x, y, z)); 		//(0,0,0)
		newVertices.Add(new Vector3(x+1, y, z));	//(1,0,0)
		newVertices.Add(new Vector3(x+1, y-1, z));	//(1,-1,0)

		Vector2 texturePosition = setSideTextures(x,y,z,block);

		Cube(texturePosition);

		//guarda las esquinas de la textura que esta en el atlas
		
	}

	void CubeWest(int x, int y , int z, byte block){//la variable typo byte nos indicara cual textura va a ser
		//agregamos 4 puntos al arreglo de vertices de la superficue tope del cubo (supongamos que le pasamos el origen de coordenadas)
		newVertices.Add(new Vector3(x, y-1, z+1)); 	//(0,-1,1)
		newVertices.Add(new Vector3(x, y, z+1)); 	//(0,0,1)
		newVertices.Add(new Vector3(x, y, z));		//(0,0,0)
		newVertices.Add(new Vector3(x, y-1, z));	//(0,-1,0)
		
		Vector2 texturePosition = setSideTextures(x,y,z,block);

		Cube(texturePosition);

		//guarda las esquinas de la textura que esta en el atlas
		
	}

	void CubeBottom(int x, int y , int z, byte block){//la variable typo byte nos indicara cual textura va a ser
		//agregamos 4 puntos al arreglo de vertices de la superficue tope del cubo (supongamos que le pasamos el origen de coordenadas)
		newVertices.Add(new Vector3(x, y-1, z)); 		//(0,1,0)
		newVertices.Add(new Vector3(x+1, y-1, z)); 		//(1,-1,0)
		newVertices.Add(new Vector3(x+1, y-1, z+1));	//(1,-1,1)
		newVertices.Add(new Vector3(x, y-1, z+1));		//(0,-1,1)

		Vector2 texturePosition = setSideTextures(x,y,z,block);

		Cube(texturePosition);

		//guarda las esquinas de la textura que esta en el atlas
		
	}

	public Vector2 setSideTextures(int x, int y, int z, byte block){
		Vector2 texturePos = new Vector2(0,0);
		if(block == (byte)TextureType.rock.GetHashCode()){
			texturePos = rock;
		}else if(block == (byte)TextureType.grass.GetHashCode()){
			texturePos = grassSide;
		}

		return texturePos;
	}

	void Cube(Vector2 texturePosition){
		//arregar los triangulos
		//primer triangulo
		newTriangles.Add(faceCount * 4); 		//1
		newTriangles.Add(faceCount * 4 + 1);	//2
		newTriangles.Add(faceCount * 4 + 2);	//3
		//segundo triangulo
		newTriangles.Add(faceCount * 4);		//1
		newTriangles.Add(faceCount * 4 + 2);	//3
		newTriangles.Add(faceCount * 4 + 3);	//4

		newUV.Add(new Vector2(textureWidth * texturePosition.x + textureWidth, textureWidth * texturePosition.y));
		newUV.Add(new Vector2(textureWidth * texturePosition.x + textureWidth, textureWidth *texturePosition.y + textureWidth));
		newUV.Add(new Vector2(textureWidth * texturePosition.x, textureWidth * texturePosition.y + textureWidth));
		newUV.Add(new Vector2(textureWidth * texturePosition.x, textureWidth * texturePosition.y));

		//facetount
		//cada vez que se crea una cara adicionamos, cuando se terminan de crear las caras del cubo faceCount volvera a cero
		faceCount++;

	}

	byte Block(int x, int y, int z){
		return world.block(x + chunkX, y + chunkY, z + chunkZ);
	}
	
}
