﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class CharacterControl : MonoBehaviour {
	[SerializeField] int moveSpeed;
	[SerializeField] int jumHeight;
	[SerializeField] private LayerMask GroundLayer;
	private Rigidbody charRigidBody;
	private Animator anim;
	private bool isGrounded;
	private AudioSource audioSource;
	// Use this for initialization
	void Start () {
		isGrounded = false;
		charRigidBody = GetComponent<Rigidbody>();
		anim = GetComponent<Animator>();
		audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {

		transform.position += transform.forward * CrossPlatformInputManager.GetAxis("Vertical") * moveSpeed * Time.deltaTime;
		
		if(CrossPlatformInputManager.GetAxis("Horizontal") != 0 || CrossPlatformInputManager.GetAxis("Vertical") != 0){
			anim.SetBool("isWalking",true);
			transform.rotation *= Quaternion.Euler(0f, CrossPlatformInputManager.GetAxis("Horizontal")*180*Time.deltaTime, 0f);
		}else{
			anim.SetBool("isWalking",false);
		}

		if(GameManager.Instance.IsJumping && IsGrounded()){
			anim.SetTrigger("jum");
			audioSource.PlayOneShot(AudioManager.Instance.Jump);
			charRigidBody.AddForce(Vector3.up*jumHeight, ForceMode.Impulse);
			GameManager.Instance.IsJumping = false;
		}

		if(GameManager.Instance.IsPunching){
			anim.SetTrigger("punch");
			audioSource.PlayOneShot(AudioManager.Instance.Hit);
			//reemplaza el bloque por aire
			ModifyTerrain.Instance.DestroyBlock(10f,(byte)TextureType.air.GetHashCode());
			GameManager.Instance.IsPunching = false;
		}

		if(GameManager.Instance.IsBuilding){
			anim.SetTrigger("punch");
			audioSource.PlayOneShot(AudioManager.Instance.Build);
			//despues se reemplazaria por un sistema de inventario
			ModifyTerrain.Instance.AddBlock(10f,(byte)TextureType.rock.GetHashCode());
			GameManager.Instance.IsBuilding = false;
		}
	}

	public bool IsGrounded(){
		RaycastHit hit;
		float distanceGround = 0;
		if(Physics.Raycast(transform.position, -transform.up, out hit, Mathf.Infinity))
			distanceGround = hit.distance;
		if(distanceGround <= 0.1){
			isGrounded = true;
		}else{
			isGrounded = false;
		}

		return isGrounded;
	}
}
