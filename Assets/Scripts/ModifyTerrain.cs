﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModifyTerrain : Singleton<ModifyTerrain> {
	private World world;
	private GameObject character;

	[SerializeField] float distToLoad;
	[SerializeField] float distToDestroy;

	// Use this for initialization
	void Start () {
		world = gameObject.GetComponent("World") as World;
		character = GameObject.FindGameObjectWithTag("Player");
	}
	
	void Update(){
		LoadChunks(GameObject.FindGameObjectWithTag("Player").transform.position,distToLoad,distToDestroy);
	}

	//usa raycast para ubicar al bloque y hacer un llamado a la funcion que lo destruye
	public void DestroyBlock(float range, byte block){
		Ray ray = new Ray(character.transform.position,character.transform.forward);
		RaycastHit hit;
		if(Physics.Raycast(ray,out hit)){
			if(hit.distance < range){
				DestroyBlockAt(hit,block);
			}
		}
	}
	//usa raycast para ubicar al bloque y hacer un llamado a la funcion que lo agrega
	public void AddBlock(float range, byte block){
		Ray ray = new Ray(character.transform.position,character.transform.forward);
		RaycastHit hit;
		if(Physics.Raycast(ray,out hit)){
			if(hit.distance < range){
				AddBlockAt(hit,block);
			}
		}
	}

	//quita bloques
	public void DestroyBlockAt(RaycastHit hit,byte block){
		Vector3 position = hit.point;
		position += (hit.normal * -0.5f);
		SetBlockAt(position,block);
	}

	//reemplaza un bloque
	public void AddBlockAt(RaycastHit hit, byte block){
		Vector3 position = hit.point;
		position += (hit.normal * 0.5f);
		SetBlockAt(position, block);
	}

	public void SetBlockAt(Vector3 position, byte block){
		int x = Mathf.RoundToInt(position.x);
		int y = Mathf.RoundToInt(position.y);
		int z = Mathf.RoundToInt(position.z);

		world.WorldData[x,y,z] = block;
		UpdateChunkAt(x,y,z);
	}

	public void UpdateChunkAt(int x,int y, int z){
		//para saber que pedazo vamos a actualizar
		int updateX = Mathf.FloorToInt(x/world.ChunkSize);
		int updateY = Mathf.FloorToInt(y/world.ChunkSize);
		int updateZ = Mathf.FloorToInt(z/world.ChunkSize);
		//el pedazo es actualizado
		world.Chunks[updateX,updateY,updateZ].IsUpdate = true;

		//x actualizamos el pedazo actual y el pedazo vecino
		if(x - (world.ChunkSize * updateX) == 0 && updateX != 0){
			world.Chunks[updateX -1 , updateY, updateZ].IsUpdate = true;
		}
		if(x -(world.ChunkSize * updateX)== 15 && updateX != world.Chunks.GetLength(0) - 1){
			world.Chunks[updateX + 1, updateY, updateZ].IsUpdate = true;
		}

		//y
		if(y - (world.ChunkSize * updateY) == 0 && updateY != 0){
			world.Chunks[updateX  , updateY - 1, updateZ].IsUpdate = true;
		}
		if(y -(world.ChunkSize * updateY)== 15 && updateY != world.Chunks.GetLength(1) - 1){
			world.Chunks[updateX, updateY + 1, updateZ].IsUpdate = true;
		}

		//z
		if(z - (world.ChunkSize * updateZ) == 0 && updateZ != 0){
			world.Chunks[updateX  , updateY, updateZ - 1].IsUpdate = true;
		}
		if(z -(world.ChunkSize * updateZ)== 15 && updateZ != world.Chunks.GetLength(2) - 1){
			world.Chunks[updateX, updateY, updateZ + 1].IsUpdate = true;
		}
	}
	
	//funcion para generar y destruir los bloques invocando las funciones de GenerateChunk y DestroyChunk inspeccionando la distancia del player
	//y el tamaño del mundo 
	public void LoadChunks(Vector3 playerPosition, float distanceToLoad, float distanceToDestroy){
	
		for(int x = 0; x < world.Chunks.GetLength(0); x++){

			for(int z = 0; z < world.Chunks.GetLength(2);z++){
				//calcular distancia entre el pedazo y el player
				float dist = Vector2.Distance(new Vector2(x * world.ChunkSize, z * world.ChunkSize), new Vector2(playerPosition.x, playerPosition.z));
				//si estoy en el rago para cargar
				if(dist <distanceToLoad){
					//si no hay pedazos, generalos
					if(world.Chunks[x,0,z] == null){
						world.GenerateChunk(x,z);
					}
				//si estoy alejado del rango para destruir
				}else if(dist > distanceToDestroy){
					//si hay pedazos cargados, destruyelos
					if(world.Chunks[x,0,z] != null){
						world.DestroyChunk(x,z);
					}
				}
			}
		}
	}


}
