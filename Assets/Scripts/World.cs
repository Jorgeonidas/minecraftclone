﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Noise;

public class World : MonoBehaviour {
	[SerializeField] GameObject chunck;
	[SerializeField] int worldX = 16;
	[SerializeField] int worldY = 16;
	[SerializeField] int worldZ = 16;
	[SerializeField] int chunkSize = 16;

	private byte[,,] worldData;
	private Chunk[,,] chunks;

	public int ChunkSize{
		get{
			return chunkSize;
		}
	}

	public Chunk[,,] Chunks{
		get{
			return chunks;
		}
	}

	public byte[,,] WorldData{
		get{
			return worldData;
		}
		set{
			worldData = value;
		}
	}

	// Use this for initialization
	void Start () {
		worldData = new byte[worldX,worldY,worldZ];

		for( int x = 0; x < worldX; x++){
			for( int z = 0; z < worldZ; z++){
				//generaremos los valores de perling antes de asignar la altura
				//base de roca (y = 0);
				int rock = PerlinNoise(x, 0, z, 10f, 3f, 1.2f);
				//montañas
				rock += PerlinNoise(x, 200, z, 20f, 8f, 0f) + 10;//le suma 10 al valor de retorno
				//grama
				int grass = PerlinNoise(x,100,z,50f,30f,0) + 1;//asegura que la altura minima para colocar grama es 1
				for( int y = 0; y < worldY; y++){
					//asinga textura basado en las alturas generadas por el ruido de perlin
					if(y <= rock){
						worldData[x,y,z] = (byte)TextureType.grass.GetHashCode();
					}else if ( y <= grass){
						worldData[x,y,z] = (byte)TextureType.rock.GetHashCode();
					}
						 					
				}
			}
		}
		//cuantos pedazos vamos a tener 
		chunks = new Chunk[Mathf.FloorToInt(worldX/chunkSize),Mathf.FloorToInt(worldY/chunkSize),Mathf.FloorToInt(worldZ/chunkSize)];	
		//instanciaremos los pedazos
		//en el arreglo multidimensional Chunks[x,y,z]
		//(0) x
		//(1) y
		//(2) z
		for(int x = 0; x < chunks.GetLength(0); x++){
			for(int y = 0; y < chunks.GetLength(1); y++){
				for(int z = 0; z < chunks.GetLength(2); z++){
					//instanciar pedazo
					GameObject newChunk = Instantiate(chunck,new Vector3(x*chunkSize-0.5f, y*chunkSize+0.5f, z*chunkSize-0.5f), new Quaternion(0,0,0,0)) as GameObject;
					chunks[x,y,z] = newChunk.GetComponent("Chunk") as Chunk;
					//almacenar la data del pedazo
					chunks[x,y,z].WorldGameObject = gameObject;
					chunks[x,y,z].ChunkSize = chunkSize;
					chunks[x,y,z].ChunkX = x *chunkSize;
					chunks[x,y,z].ChunkY = y *chunkSize;  
					chunks[x,y,z].ChunkZ = z *chunkSize;   

				}	
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	//retornara la altura aleatoreamente generada
	public int PerlinNoise(int x, int y , int z, float scale, float height, float power){
		float perlinValue;
		perlinValue = Noise.Noise.GetNoise((double)x/scale, (double)y/scale, (double)z/scale);
		//la nueva altura generada 
		perlinValue *= height;

		if(power != 0 ){
			perlinValue = Mathf.Pow(perlinValue,power);
		}

		return (int)perlinValue;
	}
	//retorna la data del bloque
	public byte block(int x, int y, int z){
		//si se sale de los limites de nuestro mundo 
		if(x >= worldX || x < 0 || y >= worldY || y < 0 || z >= worldZ || z < 0){
			return (byte)TextureType.rock.GetHashCode();
		}
		//aca donde retorno el valor de la textura
		return worldData[x, y, z];
	}
	//generar pedazos mientras avanzamos
	public void GenerateChunk(int x, int z){
		for(int y = 0; y < chunks.GetLength(1); y++){
            //instanciar pedazo
            GameObject newChunk = Instantiate(chunck, new Vector3(x * chunkSize - 0.5f, y * chunkSize + 0.5f, z * chunkSize - 0.5f), new Quaternion(0, 0, 0, 0)) as GameObject;
            chunks[x, y, z] = newChunk.GetComponent("Chunk") as Chunk;
            //almacenar la data del pedazo
            chunks[x, y, z].WorldGameObject = gameObject;
			chunks[x,y,z].ChunkSize = chunkSize;
			chunks[x,y,z].ChunkX = x *chunkSize;
			chunks[x,y,z].ChunkY = y *chunkSize;  
			chunks[x,y,z].ChunkZ = z *chunkSize;   
		}				
	}

	//destruir pedazos mientras avanzamos
	public void DestroyChunk(int x, int z){
		for(int y = 0; y < chunks.GetLength(1); y++){
			Object.Destroy(Chunks[x, y, z].gameObject);
		}	
	}
}
